
//fetch is a way to retrieve data from an API. The '/then function' waits for the data to be fulfilleed before it moves on to other code.
let posts = fetch('https://jsonplaceholder.typicode.com/posts') //fetch is function returns a promise that is consumable using the '/then' syntax as well

//from response then convert from JSON to JS array
	.then((response) => response.json())
	.then(json => console.log(json))


console.log(posts)

//fetchData
// Async & await is the javascript ES6 equivalent of the .then syntax. It allows us to write more streamline/clean code while utilizing an API fetching functionality. It makes our code asynchronous.
/*async function fetchPosts(){
	let result = await fetch('https://jsonplaceholder.typicode.com/posts')

	console.log(result)

	let json = await result.json()

	console.log(json)
}

fetchPosts()*/


// Get a single data post from the API
/*	
	We can specify the method using a second object argument within the fetch function. Besides the method, we can also specify the headers and the body of the request.
*/
/*let post = fetch('https://jsonplaceholder.typicode.com/posts/10')
			.then((response) => response.json())
			.then((data) => console.log(data))*/


/*let inputField = 'Earl'
body: JSON.stringify({
		firstName: inputField,
*/
//console.log(post)


/*fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'POST', 
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: 'New Post',
		body: 'Hello World!'
	})
})
.then((response) => response.json())
.then((data) => console.log(data))*/


fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'POST', 
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		userId: 1,
		title: 'New Post',
		body: 'Hello World!'
	})
})
.then((response) => response.json())
.then((data) => console.log(data))

//Update existing
// PUT 
/*
	1. ID of the post to be updated
	2. New body
*/
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'PUT',
	headers: {
		'Content-Type': 'application/json'
	},
	body: JSON.stringify({
		title: 'Updated post'
	})
})

.then((response) => response.json())
.then((data) => console.log(data))

//Delete a single posst using the DELETE method
/*
	Make sure you specify the ID of the post to be deleted.
	No need for a header and body since we are not passing any data to this request
*/

fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE',
	/*body: JSON.stringify({
		idsToBeDeleted: [1, 2, 6]
	})*/
})
.then((response) => response.json())
.then((data) => console.log(data))


// We can filter/query the URL using the '?' syntax after the endpoint
fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then((response) => response.json())
.then((data) => console.log(data))


//We can fetch the comments from a specific post by using the ID of the post as well as using the '/comments' endpoint
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then((response) => response.json())
.then((data) => console.log(data))



